#include <iostream>
#include <iomanip>
#include <math.h>
#include "shaman.h"

/*
 * Calcule l'intégrale d'une fonction entre deux bornes par la méthode des rectangles.
 * inf:  borne gauche de l'intervalle d'intégration
 * sup:  borne droite
 * n:   nombre de rectangles
 */
Sfloat testVerrou(int n)
{
    Sfloat val = (float) 0.1;
    Sfloat sum = 0;

    for (int i =0; i< n;i++){
        sum+=val*i;
    }

    return sum;
}

// SOURCE ENV.SH
// source /home/sphinx06/Documents/ENSIIE/3A/GRILLECALCUL/verrou/env.sh
int main()
{
    Sfloat result=testVerrou(1000);
    std::cout<<"Résultat du calcul: " << result << std::endl;
    std::cout<<"Prédiction de l'erreur: " << result.error << std::endl;
    return 0;
}