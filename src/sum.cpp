#include <iostream>
#include <iomanip>
#include <math.h>

/*
 * Calcule l'intégrale d'une fonction entre deux bornes par la méthode des rectangles.
 * inf:  borne gauche de l'intervalle d'intégration
 * sup:  borne droite
 * n:   nombre de rectangles
 */
float testVerrou(unsigned int n)
{
    float val = 0.1;
    float sum = 0;

    for (int i =0; i< n;i++){
        sum+=val*i;
    }

    return sum;
}

// SOURCE ENV.SH
// source /home/sphinx06/Documents/ENSIIE/3A/GRILLECALCUL/verrou/env.sh
int main(int argc, char** argv)
{
    //float step = (argc > 1) ? std::atof(argv[1]) : 1.1;
    //testConvergence(step);
    float result=testVerrou(1000);
    std::cout<<std::setprecision(17)<<result;
    return 0;
}