#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <string>
#include "shaman.h"
using namespace std;

/*
 * Calcule l'intégrale d'une fonction entre deux bornes par la méthode des rectangles.
 * inf:  borne gauche de l'intervalle d'intégration
 * sup:  borne droite
 * n:   nombre de rectangles
 */
Sfloat integrate(Sfloat inf, Sfloat sup, unsigned int n)
{
    const Sfloat stepsize = (sup - inf) / n;

    // Boucle sur les rectangles d'intégration
    Sfloat sum = 0.f;
    for (Sfloat x = inf + stepsize/2; x < sup; x += stepsize)
    {
        sum += stepsize * cos(x);
    }

    return sum;
}

/*
 * Teste la convergence du calcul d'intégrale en fonction du nombre de rectangles.
 *
 * Cas-test : intégrale de cos entre 0 et pi/2 (valeur exacte 1).
 * Réalisé pour une suite croissante (~géométrique) de nombre de rectangles.
 *
 * step:  facteur entre deux nombre de rectangles testés
 */
void testConvergence(const Sfloat& step)
{
    Sfloat inf = 0;
    Sfloat sup = float(M_PI_2); // pi/2
    Sfloat expectedResult = 1;

    string resultPath = "results/";
    string filename = "original_result_shaman.txt";

    ofstream myfile (resultPath + filename);
    if (myfile.is_open())
    {
        for (unsigned int n = 1; n <= 100000; n = (unsigned int) ceil(step*n))
        {
            // Résultat de l'intégration
            Sfloat result = integrate(inf, sup, n);

            // Erreur par rapport à la valeur attendue
            Sfloat error = abs(expectedResult - result);
            myfile << std::scientific << std::setprecision(17)
                    << n << ';' // nombre de rectangles
                    << result << ';' // affichage du résultat par shaman (uniquement les chiffres significatifs)
                    << abs(result.error)  // calcul de l'erreur par Shaman
                    << endl;
        }
        myfile.close();
    }
    else cout << "Unable to open file: " << resultPath << filename << endl;

}

/*
 * Fonction principale :
 * le facteur de croissance du nombre de rectangles testés est le premier argument.
 * La valeur 10 est choisie par défaut si aucun argument n'est fourni.
 */
int main(int argc, char** argv)
{   
    float step = (argc > 1) ? std::atof(argv[1]) : 1.1;
    testConvergence(step);

    return 0;
}

