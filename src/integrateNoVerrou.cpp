#include <iostream>
#include <iomanip>
#include <math.h>
#include <fstream>
#include <ctime>
#include <sstream>
#include <string>
using namespace std;

/*
 * Calcule l'intégrale d'une fonction entre deux bornes par la méthode des rectangles.
 * inf:  borne gauche de l'intervalle d'intégration
 * sup:  borne droite
 * n:   nombre de rectangles
 */
float integrate(float inf, float sup, unsigned int n)
{
    const float stepsize = (sup - inf) / n;

    // Boucle sur les rectangles d'intégration
    float sum = 0.f;
    for (float x = inf + stepsize/2; x < sup; x += stepsize)
    {
        sum += stepsize * cos(x);
    }

    return sum;
}

/*
 * Teste la convergence du calcul d'intégrale en fonction du nombre de rectangles.
 *
 * Cas-test : intégrale de cos entre 0 et pi/2 (valeur exacte 1).
 * Réalisé pour une suite croissante (~géométrique) de nombre de rectangles.
 *
 * step:  facteur entre deux nombre de rectangles testés
 */
void testConvergence(const float& step)
{
    float inf = 0;
    float sup = float(M_PI_2); // pi/2
    float expectedResult = 1;

    string resultPath = "results/";
    string filename = "original_result.txt";

    ofstream myfile (resultPath + filename);
    if (myfile.is_open())
    {
        for (unsigned int n = 1; n <= 100000; n = (unsigned int) ceil(step*n))
        {
            // Résultat de l'intégration
            float result = integrate(inf, sup, n);

            // Erreur par rapport à la valeur attendue
            float error = abs(expectedResult - result);
            myfile << std::scientific << std::setprecision(17)
                    << n << ';' // nombre de rectangles
                    << (float) result << ';' // résultat
                    << (float) error // erreur
                    //<< result << '\t' // affichage du résultat par shaman (uniquement les chiffres significatifs)
                    //<< std::abs(result.error) << '\t' // calcul de l'erreur par Shaman
                    << std::endl; 
        }
        myfile.close();
    }
    else cout << "Unable to open file";

}

/*
 * Fonction principale :
 * le facteur de croissance du nombre de rectangles testés est le premier argument.
 * La valeur 10 est choisie par défaut si aucun argument n'est fourni.
 */
int main(int argc, char** argv)
{   
    float step = (argc > 1) ? std::atof(argv[1]) : 1.1;
    testConvergence(step);

    return 0;
}

