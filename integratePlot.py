#!/usr/bin/env python3
import glob, os
import pandas as pd
import statistics 
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

#=============================
# Déclaration des variables
#=============================
cols = ["rect","result","error"]

li = []
liShaman = []
allDevStd = []
allDevStdShaman = []

original_data = None
original_shaman_data = None
nbline = None

#=============================
# Récupération des résultats
#=============================
# Lecture du dossier
os.chdir("./results")

# Récupération de tous les résultats VERROU
for file in glob.glob("result_*.txt"):
    data = pd.read_csv(file,
                    sep=";",
                    header=None,
                    engine='python')
    data.columns = cols
    li.append(data)
    if(not nbline or data.shape[0] < nbline):
        nbline = data.shape[0]

# Récupération de tous les résultats VERROU version SHAMAN
# Récupération du résultat pdu fichier original
original_shaman_data = pd.read_csv("original_result_shaman.txt",
                    sep=";",
                    header=None,
                    engine='python')
original_shaman_data.columns = cols

# Récupération du résultat pdu fichier original
original_data = pd.read_csv("original_result.txt",
                    sep=";",
                    header=None,
                    engine='python')
original_data.columns = cols

#=============================
# Lecture des résultats et calcul devstd
#=============================

# Lecture des résultats
for i in range(0,nbline):
    dvtsum = []
    for data in li:
        # Exception possible si nombres de lignes inférieure à la donnée originale
        try:
            dvtsum.append(data["result"].values[i])
        except:
            continue
    allDevStd.append(statistics.stdev(dvtsum))

#=============================
# Affichage
#=============================

# Print result
#print(original_data["error"].values)
#print(allDevStd)

# Plotting
# Mise à l'echelle loglog
plt.xscale('log')
plt.yscale('log')
# Légende axis
plt.xlabel("Nombre de rectangles")
plt.ylabel("Erreur")
# Scatter plot
plt.scatter(original_data["rect"].values[:nbline],allDevStd , c='blue' ,label="Erreur : DevStd calculée")
plt.scatter(original_data["rect"].values,original_shaman_data["error"].values , c='red' ,label="Erreur : sans verrou SHAMAN")
plt.scatter(original_data["rect"].values,original_data["error"].values , c='green' , label="Erreur: sans verrou")
plt.legend()
plt.show()



